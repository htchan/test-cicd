var http = require('http');

const listener = (req, res) => {
    res.writeHead(200);
    res.write('wow, you find me!!');
    res.end();
}

const server = http.createServer(listener);

server.listen(10086);
